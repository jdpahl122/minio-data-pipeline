import requests
from minio import Minio
from minio.error import S3Error
import pandas as pd
from datetime import datetime
import io

def fetch_data():
    # Fetch data from an API and return a DataFrame
    url = 'https://jsonplaceholder.typicode.com/posts'
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        df = pd.DataFrame(data)
        print("Data fetched successfully.")
        return df
    else:
        print("Failed to fetch data.")
        return None

def partition_and_upload_to_minio(dataframe, chunk_size):
    # Minio configurations
    minio_client = Minio('minio.s3.svc.cluster.local:9000/',
                         access_key='minio',
                         secret_key='minio123',
                         secure=False)

    try:
        chunk_num = 0
        for start in range(0, len(dataframe), chunk_size):
            chunk = dataframe.iloc[start:start + chunk_size]
            csv_buffer = io.StringIO()
            chunk.to_csv(csv_buffer, index=False)
            csv_data = csv_buffer.getvalue().encode('utf-8')
            dt = datetime.now()
            chunk_filename = f'{dt}part_{chunk_num}.json'
            minio_client.put_object('dev-bucket', chunk_filename, io.BytesIO(csv_data), len(csv_data))
            print(f"Uploaded {chunk_filename} to Minio.")
            chunk_num += 1
    except S3Error as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    # Fetch data from API and convert to DataFrame
    df = fetch_data()
    
    if df is not None:
        chunk_size = 1000 
        partition_and_upload_to_minio(df, chunk_size)
