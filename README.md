# minio-data-pipeline

## Setup

Create a fork of this repo [Fork](https://gitlab.com/jdpahl122/minio-data-pipeline/-/forks/new)

### Install Minio
On your local machine, install mino
1. `cd k8s`
2. `k apply -f minio.yaml`
3. Set your context to the s3 namespace (I have an alias set up, but you can use `kubectl config set-context --current --namespace s3`)
4. After minio installation is complete, navigate to the minio url create a dev-bucket (I used port-forward `kubectl port-forward svc/minio 9000`)
![](images/bucket1.png)
![](images/bucket2.png)

### Test python data-transfer.py locally (skip to Runner Setup if you want to go right into pipeline)

1. Depending on how your k8s cluster is set up, update the minio url accordingly (I used port forward and 127.0.0.1:9000)
2. install requirements `pip install -r requirements.txt`
3. `python data-transfer.py`

### How to run tests

`python -m unittest data_transfer/tests/test_data_transfer.py`

### Runner setup on Gitlab (gitlab.com)

1. Navigate to settings/cicd/runners on gitlab.com
2. Expand the runners tab and create a new project runner
3. Set up the runner depending on your local configuation (mac / linux / etc)
4. Make sure to create a tag called `local-runner`
5. Copy the runner token (you will need this for the next step)


### Runner setup on local

1. Create a runner for your new repo and grab the runnerToken `settings/cicd/runners`
2. Update the k8s/runner_values.yaml with your gitlabUrl and runnerToken
3. On your k8s instance, run the gitlab runner helm chart to install locally 
```
helm install --namespace gitlab-runner gitlab-runner -f k8s/runner_values.yaml gitlab/gitlab-runner --create-namespace
```

## Running the pipeline

Assuming that the above steps succeeded without any errors, go ahead and navigate back to your repo on [Gitlab](https://www.gitlab.com)

Navigate to pipelines, then click run pipeline to run through the pipeline. 

At this point, tests should succeed and you should should see data in your minio bucket

## Follow on steps

A couple of things that could be improved

1. Using ENV variables (Minio url/key/pw, url for data api) -- These are values that should never be hardcoded, but I wanted to spare the reviewer having to add in all the environment variables separately
2. Setting up a job schedule -- If this were a production data pipeling, we would most likely want this to run on a routine schedule to pull new values.
3. Probably set up the minio operator and tennants. This seemed a bit overkill for this project, so I defaulted to the more straigforward minio bucket deployment.
4. This wouldn't be the best solution for extremely large datasets, since it would require a large amount of memory. I would probably use something like spark / glue depending on size. 
5. Minio wouldn't be my first choice for production, since it adds overhead and s3 or google buckets are relatively cheap solutions

## Output of success

1. Pipeline Run
![](images/pipeline-success.png)
2. Pipeline Logs
![](images/pipeline-log.png)
3. Logs for local runner
![](images/k8s-log.png)
4. Bucket with data after run
![](images/bucket-success.png)



