import unittest
from unittest.mock import MagicMock
from data_transfer import fetch_data, partition_and_upload_to_minio

class TestDataExport(unittest.TestCase):
    def setUp(self):
        # Mocking DataFrame with test data for fetch_data() function
        self.test_data = [
            {'Column1': 1, 'Column2': 'A'},
            {'Column1': 2, 'Column2': 'B'},
            # Add more test data as needed
        ]

    def test_fetch_data_success(self):
        # Mocking the requests library and simulating a successful API call
        response_mock = MagicMock()
        response_mock.status_code = 200
        response_mock.json.return_value = self.test_data
        with unittest.mock.patch('requests.get', return_value=response_mock):
            df = fetch_data()
            self.assertIsNotNone(df)
            # Add more assertions to validate the fetched DataFrame as needed

    def test_partition_and_upload(self):
        # Mocking DataFrame to test partition_and_upload_to_minio() function
        mock_df = MagicMock()
        mock_df.iloc.return_value = self.test_data  # Simulating DataFrame for testing
        # Mocking Minio client
        minio_client_mock = MagicMock()
        with unittest.mock.patch('data_transfer.Minio', return_value=minio_client_mock):
            partition_and_upload_to_minio(mock_df, chunk_size=2)  # Assuming chunk size as 2
        
        # Add assertions here to check if the Minio upload function is called with correct parameters

if __name__ == '__main__':
    unittest.main()
